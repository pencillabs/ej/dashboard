FROM python:3.7
COPY . /dashboard
WORKDIR  /dashboard
RUN cp /dashboard/configs/client_secrets.json /tmp/
RUN cp /dashboard/configs/analyticsreporting.dat /tmp/.analyticsreporting.dat
RUN pip install -r requirements.txt
